function generate(items) {
    var newItems = [];
    for (var i = 0; i < items.length; i++) {
        var item = poolSelection(items);
        newItems[i] = item;
    }
    return newItems;
}

function normalizeFitness(items) {
    for (var i = 0; i < items.length; i++) {
        items[i].score = pow(items[i].score, 2);
    }

    var sum = 0;
    for (var i = 0; i < items.length; i++) {
        sum += items[i].score;
    }

    for (var i = 0; i < items.length; i++) {
        items[i].fitness = items[i].score / sum;
    }
}

function poolSelection(items) {
    var index = 0;
    var r = random(1);

    while (r > 0) {
        r -= items[index].fitness;
        index += 1;
    }
    index -= 1;

    return items[index].copy();
}